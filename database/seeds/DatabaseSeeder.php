<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     /*   DB::table('users')->insert([
            'name'              => 'Administrador',
            'email'             => 'admin@admin.com',
            'password'          => bcrypt('1234'),
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now(),
        ]);*/

        DB::table('settings')->insert([
            'name' => 'launch_date',
            'value' => date('Y') . '-12-31',
        ]);

        DB::table('settings')->insert([
            'name' => 'fb_page',
            'value' => 'https://www.facebook.com',
        ]);

        DB::table('settings')->insert([
            'name' => 'gplus_page',
            'value' => 'https://www.google.com',
        ]);

        DB::table('settings')->insert([
            'name' => 'tw_page',
            'value' => 'https://www.twiter.com',
        ]);

        DB::table('settings')->insert([
            'name' => 'insta_page',
            'value' => 'https://www.instagram.com',
        ]);

        DB::table('settings')->insert([
            'name' => 'home_message',
            'value' => 'EN CONSTRUCCION',
        ]);

        DB::table('settings')->insert([
            'name' => 'brand_message',
            'value' => 'Estamos preparando todo para comenzar!!!!',
        ]);

        DB::table('backgrounds')->insert([
            'image' => '1.jpg',
            'active' => 1,
        ]);

        DB::table('backgrounds')->insert([
            'image' => '2.jpg',
            'active' => 0,
        ]);

        DB::table('backgrounds')->insert([
            'image' => '3.jpg',
            'active' => 0,
        ]);

        DB::table('backgrounds')->insert([
            'image' => '4.jpg',
            'active' => 0,
        ]);
        DB::table('backgrounds')->insert([
            'image' => '5.jpg',
            'active' => 0,
        ]);
        DB::table('backgrounds')->insert([
            'image' => '6.jpg',
            'active' => 0,
        ]);
        DB::table('backgrounds')->insert([
            'image' => '7.jpg',
            'active' => 0,
        ]);
    }
}
